/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstiter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/04 03:34:50 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/04 15:56:52 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

void	ft_lstiter(t_list *lst, void (*f)(t_list *elem))
{
	t_list *buf;

	if (!(lst && f && *f))
		return ;
	while (lst)
	{
		buf = lst->next;
		f(lst);
		lst = buf;
	}
}
