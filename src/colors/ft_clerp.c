/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_clerp.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 22:46:23 by dtitenko          #+#    #+#             */
/*   Updated: 2017/10/07 22:46:24 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_colors.h"
#include "ft_math.h"

t_color		ft_clerp(t_color c1, t_color c2, double t)
{
	t_color c;

	if (c1.color == c2.color)
		return (c1);
	(t < 0.0) ? t = 0.0 : 0;
	(t > 1.0) ? t = 1.0 : 0;
	c.argb.r = (char)ft_ilerp(c1.argb.r, c2.argb.r, t);
	c.argb.b = (char)ft_ilerp(c1.argb.b, c2.argb.b, t);
	c.argb.g = (char)ft_ilerp(c1.argb.g, c2.argb.g, t);
	c.argb.a = (char)0x0;
	return (c);
}

int			ft_iclerp(int c1, int c2, double t)
{
	return (ft_clerp((t_color)c1, (t_color)c2, t).color);
}
