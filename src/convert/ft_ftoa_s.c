/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ftoa_s.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 00:08:10 by dtitenko          #+#    #+#             */
/*   Updated: 2017/02/20 00:11:30 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static long double	ft_get_exp(long double *n)
{
	long double exp;

	exp = 0;
	while (!(ABS(*n) < 10.0 && ABS(*n) >= 1.0))
		if (ABS(*n) >= 10.0)
		{
			(*n) /= 10.0;
			exp += (long double)1.0;
		}
		else
		{
			(*n) *= (long double)10.0;
			exp -= (long double)1.0;
		}
	return (exp);
}

char				*ft_ftoa_s(long double n, int perc, int decpoint)
{
	long double	exp;
	char		*mantissa;
	char		*exponent;
	char		*num;

	exp = ft_get_exp(&n);
	mantissa = ft_ftoa(n, perc, decpoint);
	exponent = ft_itoa(ABS(exp));
	n = ft_strlen(mantissa) + ft_strlen(exponent) + 1;
	n += (exp < 10 && exp >= 0) ? 2 : 0;
	num = ft_strnew((size_t)n);
	ft_strcat(num, mantissa);
	ft_strcat(num, "e");
	(exp >= 0) ? ft_strcat(num, "+") : ft_strcat(num, "-");
	(ABS(exp) < 10 && ABS(exp) >= 0) ? ft_strcat(num, "0") : 0;
	ft_strcat(num, exponent);
	ft_strdel(&mantissa);
	ft_strdel(&exponent);
	return (num);
}
