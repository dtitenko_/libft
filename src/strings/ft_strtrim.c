/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 20:19:45 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/01 20:55:49 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strtrim(char const *s)
{
	char	*ws;
	char	*ends;
	char	*ss;

	ws = " \n\t";
	if (!s)
		return (NULL);
	while (*s && ft_instr(*s, ws))
		s++;
	if (!*s)
		return ((char *)s);
	ends = (char *)s + ft_strlen(s) - 1;
	while ((char *)s != ends && ft_instr(*ends, ws))
		ends--;
	if (ends - s < 1)
		return (NULL);
	if (!(ss = ft_strnew(ends - s + 1)))
		return (NULL);
	ss = ft_strncpy(ss, s, ends - s + 1);
	return (ss);
}
