/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 18:54:15 by dtitenko          #+#    #+#             */
/*   Updated: 2016/11/30 20:05:04 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strcmp(const char *s1, const char *s2)
{
	if (!s1 && !s2)
		return (0);
	while (*s1 && *s2 && *(t_uchar *)s1 == *(t_uchar *)s2)
	{
		s1++;
		s2++;
	}
	return ((t_uchar)*s1 - (t_uchar)*s2);
}
