/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 19:34:11 by dtitenko          #+#    #+#             */
/*   Updated: 2016/11/26 19:35:07 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t nb)
{
	char			*dd;
	char			*ss;
	size_t			n;
	size_t			dstlen;

	dd = dst;
	ss = (char *)src;
	n = nb;
	while (*dd != '\0' && n-- != 0)
		dd++;
	dstlen = dd - dst;
	n = nb - dstlen;
	if (!n)
		return (dstlen + ft_strlen(ss));
	while (*ss)
	{
		if (n != 1)
		{
			*dd++ = *ss;
			n--;
		}
		ss++;
	}
	*dd = '\0';
	return (dstlen + (ss - src));
}
