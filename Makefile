# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/12/26 18:45:59 by dtitenko          #+#    #+#              #
#    Updated: 2016/12/26 18:46:32 by dtitenko         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libft
INCLUDEFOLDERS = ./includes/

SOURCES_FOLDER = src/
OBJECTS_FOLDER = obj/

SOURCES_STRINGS = \
			strings/ft_strmapi.c\
            strings/ft_strjoin.c\
            strings/ft_strdel.c\
            strings/ft_strtrim.c\
            strings/ft_isalpha.c\
            strings/ft_strrchr.c\
            strings/ft_strlcat.c\
            strings/ft_strmap.c\
            strings/ft_strncmp.c\
            strings/ft_isprint.c\
            strings/ft_strncat.c\
            strings/ft_strlen.c\
            strings/ft_strchr.c\
            strings/ft_striter.c\
            strings/ft_count_whitespaces.c\
            strings/ft_instr.c\
            strings/ft_strncpy.c\
            strings/ft_strnew.c\
            strings/ft_strcpy.c\
            strings/ft_striteri.c\
            strings/ft_strdup.c\
            strings/ft_isdigit.c\
            strings/ft_strsub.c\
            strings/ft_strclr.c\
            strings/ft_isalnum.c\
            strings/ft_isascii.c\
            strings/ft_strnstr.c\
            strings/ft_strequ.c\
            strings/ft_strsplit.c\
            strings/ft_strnequ.c\
            strings/ft_strstr.c\
            strings/ft_strcmp.c\
            strings/ft_strcat.c\
            strings/ft_strsplitdel.c


SOURCES_MEMORY = \
			memory/ft_memcmp.c\
            memory/ft_memset.c\
            memory/ft_memdel.c\
            memory/ft_memccpy.c\
            memory/ft_memmove.c\
            memory/ft_bzero.c\
            memory/ft_memcpy.c\
            memory/ft_memalloc.c\
            memory/ft_memchr.c\
            memory/ft_memdup.c


SOURCES_LISTS = \
			lists/ft_lstdel.c\
            lists/ft_lstrev.c\
            lists/ft_lstdelone.c\
            lists/ft_lstqueueadd.c\
            lists/ft_lstiter.c\
            lists/ft_lstnew.c\
            lists/ft_lstmap.c\
            lists/ft_lstadd.c\
            lists/ft_lstfreeto.c \
            lists/ft_lstlen.c

SOURCES_PRINT = \
			print/ft_putstr.c \
            print/ft_putnbrb.c \
            print/ft_putnbr.c \
            print/ft_putendl_fd.c \
            print/ft_putlnbr.c \
            print/exit_with_error.c \
            print/ft_putstr_fd.c \
            print/ft_putnbr_fd.c \
            print/ft_putchar_fd.c \
            print/ft_putchar.c \
            print/ft_putendl.c

SOURCES_CONVERT = \
			convert/ft_wctomb.c\
            convert/ft_itoa.c\
            convert/ft_atol_base.c\
            convert/ft_atof.c\
            convert/ft_ltoa_base.c\
            convert/ft_atoi.c\
            convert/ft_tolower.c\
            convert/ft_wcstombs.c\
            convert/ft_ftoa.c\
            convert/ft_ftoa_s.c\
            convert/ft_toupper.c\
            convert/ft_ulltoa_base.c\


SOURCES_MATH = \
			math/ft_abs.c \
			math/ft_lerp.c \
			math/ft_pow.c \
			math/ft_powf.c \
			math/ft_round.c \

SOURCES_INPUT = \
			input/ft_getchar.c\
			input/get_next_line.c

SOURCES_COLORS = \
			colors/color_blend_over.c\
            colors/color_blend_add.c\
            colors/fade_color.c\
            colors/color_blend_sub.c\
            colors/ft_rgb2int.c\
            colors/blend_colors.c \
            colors/ft_clerp.c

SOURCES = 	$(SOURCES_CONVERT) $(SOURCES_INPUT) $(SOURCES_LISTS) \
			$(SOURCES_MEMORY) $(SOURCES_PRINT) $(SOURCES_STRINGS) \
			$(SOURCES_COLORS) $(SOURCES_MATH)

OBJECTS = $(SOURCES:.c=.o)
OBJECTS := $(subst /,__,$(OBJECTS))
OBJECTS := $(addprefix $(OBJECTS_FOLDER), $(OBJECTS))
SOURCES := $(addprefix $(SOURCES_FOLDER),$(SOURCES))

CC = gcc
AR = ar
CFLAGS = -Wall -Werror -Wextra


# Colors

NO_COLOR =		\033[0;00m
OK_COLOR =		\033[38;5;02m
ERROR_COLOR =	\033[38;5;01m
WARN_COLOR =	\033[38;5;03m
SILENT_COLOR =	\033[38;5;04m


# Basic Rules

.PHONY: all re clean fclean

all: $(NAME)

$(OBJECTS_FOLDER)%.o:
	@mkdir -p $(OBJECTS_FOLDER)
	@$(CC) -c $(subst .o,.c,$(subst $(OBJECTS_FOLDER),$(SOURCES_FOLDER),$(subst __,/,$@))) -I$(INCLUDEFOLDERS) $(CFLAGS) $(MACROS) -o $@
	@printf "$(OK_COLOR)✓ $(NO_COLOR)"
	@echo "$(subst .o,.c,$(subst $(OBJECTS_FOLDER),$(SOURCES_FOLDER),$(subst __,/,$@)))"

$(NAME): $(OBJECTS)
	@printf "$(SILENT_COLOR)Compiling Libft...$(NO_COLOR)\n"
	@$(AR) rcv $(NAME).a $(OBJECTS)
	@printf "$(OK_COLOR)Successful ✓$(NO_COLOR)\n"

clean:
	@rm -f $(OBJECTS)
	@printf "$(SILENT_COLOR)$(NAME) : Cleaned Objects$(NO_COLOR)\n"

fclean: clean
	@rm -f $(NAME).a
	@printf "$(SILENT_COLOR)$(NAME) : Cleaned Library$(NO_COLOR)\n"

re: fclean all
